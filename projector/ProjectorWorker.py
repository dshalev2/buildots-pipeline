from common.pipline.Worker import Worker
from common.pipline.Job import Job
from external_libraries.nfov import NFOV
import imageio as im
import numpy as np
import uuid
import matplotlib.pyplot as plt

class ProjectorWorker(Worker):
    """The Projector pipeline worker"""
    
    def processJob(self, job: Job):
        img = im.imread(job.fileName)
        nfov = NFOV()
        centerPoint = np.array([0.5, .5])
        projectedImg = nfov.toNFOV(img, centerPoint)
        plt.imsave("{}/{}".format(job.outputPath, job.fileName.split("/")[-1]), projectedImg)

