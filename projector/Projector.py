from common.pipline.PipelineStageRunner import PipelineStageRunner
from common.pipline.FileSystemPipelineJobHandler import FileSystemPipelineJobHandler
from common import Configuration
from projector.ProjectorWorker import ProjectorWorker

worker = ProjectorWorker()
jobHandler = FileSystemPipelineJobHandler(worker, \
            Configuration.DOWNLOADED_IMAGES_DIR, \
            Configuration.PROJECTED_IMAGES_DIR, \
            Configuration.PROCESSING_ERRORS_DIR)
runner = PipelineStageRunner()
runner.run(jobHandler)
