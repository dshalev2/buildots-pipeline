## Buildots pipeline

### Running Instructions:

this assumes python3, pip, and virtualenv are already installed on your machine

1. `git clone https://gitlab.com/dshalev2/buildots-pipeline.git`
2. `cd buildots-pipeline`
3. create virtual env `python3 -m virtualenv env`
4. activate yout virtual env: `source env/bin/activate`
5. install reuirements: `pip install -r requirements.txt`
6. add curent path to pythonpath: `export PYTHONPATH=$PYTHONPATH:$(pwd)`
7. Finally run the workers (i suggest to run each of them in a different cli tabs, you then need to export - stage 6 in every dir): 
    ```
        env/bin/python3.9 images_downloader/ImagesDownloader.py
        env/bin/python3.9 projector/Projector.py
        env/bin/python3.9 analyzer/Analyzer.py
    ```
### Some clarifications:

* All input and output paths are configured at common/Configuration.py
* Image downloader input path is `workersInOut/images_urls_files`
* When a service fails to process a request an error file is being written to `workersInOut/errors`,
    * No retry mechanism was implemented
    * In case of image download failure i chose to fail the entire file even if he contains more urls, given more time i would have implemented a better solution
* the analyzer output json file name has the format: image_name.json, and this is how we can attach a result to the initial image
* Analyzers were implemented to run in serial given more time i would have run them in parallel (probably using threads)
* Iv'e added a sleep(2) in the workers loop so images will have time to be written to file before next service catches the file
* I'm not developing in python on a day to day basis, if someone find my coding style offensive i apologize :)

### Answere to the question:

Main issue with running more then one instance of a service is that potentialy multiple instances would work on every input file.
3 approaches to address this issue:
1. implement a locking mechanism, so that when one instance "locks" a file other instance will not choose to work on it
2. use a queue that only delivers an item to a single instance
3. partition the input files, every instance will handle a single partition (can also be done using a messaging system with consumer groups)
