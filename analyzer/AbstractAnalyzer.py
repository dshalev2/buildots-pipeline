import abc

class AbstractAnalyzer(abc.ABC):
    """An abstract class defining the analyzer interface"""

    @abc.abstractmethod
    def analyze(self, imagePath: str):
        pass

    @abc.abstractmethod
    def setNext(self, analyzer):
        pass

    @abc.abstractmethod
    def getJsonResult(self) -> str:
        pass