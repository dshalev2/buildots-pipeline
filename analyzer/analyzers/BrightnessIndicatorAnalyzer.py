from analyzer.analyzers.BaseAnalyzer import BaseAnalyzer
from PIL import Image, ImageStat
import math

class BrightnessIndicatorAnalyzer(BaseAnalyzer):
    """An analyzer for brightness indication"""
    
    ANALYSIS_NAME = "brightness_indicator"
    
    def analyze(self, imagePath: str):
        im = Image.open(imagePath)
        stat = ImageStat.Stat(im)
        r,g,b = stat.rms
        self.analysisData[self.ANALYSIS_NAME] = math.sqrt(0.241*(r**2) + 0.691*(g**2) + 0.068*(b**2))
        super().analyze(imagePath)