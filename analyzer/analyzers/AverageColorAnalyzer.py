from analyzer.analyzers.BaseAnalyzer import BaseAnalyzer
from PIL import Image

class AverageColorAnalyzer(BaseAnalyzer):
    """An analyzer for average color"""
    
    ANALYSIS_NAME = "average_color"

    def analyze(self, image: str):
        img = Image.open(image)
        average = img.resize((1, 1), Image.ANTIALIAS)
        self.analysisData[self.ANALYSIS_NAME] = average.getpixel((0, 0))
        super().analyze(image)