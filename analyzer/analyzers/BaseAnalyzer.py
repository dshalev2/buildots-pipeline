from analyzer.AbstractAnalyzer import AbstractAnalyzer
import json

class BaseAnalyzer(AbstractAnalyzer):
    """The base class for analyzers, most of the chain of responsibility code is implemented here"""

    nextAnalyzer: AbstractAnalyzer = None

    analysisData: dict = {}

    def setNext(self, analyzer: AbstractAnalyzer):
        self.nextAnalyzer = analyzer
        return analyzer
    
    def analyze(self, imagePath: str):
        if self.nextAnalyzer:
            return self.nextAnalyzer.analyze(imagePath)
        return None

    def getJsonResult(self):
        return json.dumps(self.analysisData)