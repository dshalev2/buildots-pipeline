from analyzer.analyzers.BaseAnalyzer import BaseAnalyzer
import imageio as im

class ImageDimensionsAnalyzer(BaseAnalyzer):
    """An analyzer for image dimensions"""

    ANALYSIS_NAME = "image_dimensions"

    def analyze(self, imagePath: str):
        dims = im.imread(imagePath).shape
        self.analysisData[self.ANALYSIS_NAME] = dims
        super().analyze(imagePath)
