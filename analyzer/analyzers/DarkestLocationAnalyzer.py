from analyzer.analyzers.BaseAnalyzer import BaseAnalyzer
import imageio as im
import numpy as np

class DarkestLocationAnalyzer(BaseAnalyzer):
    """An analyzer for darkest 20x20 window"""

    ANALYSIS_NAME = "darkest_20_by_20_location"

    def analyze(self, imagePath: str):
        
        # open image and convert to grayscale
        img = np.array(im.imread(imagePath, as_gray = True))
        
        # get 2d rolling windows of the image with size 20X20
        windows = self.rollingWindow(img, (20, 20))
    
        self.analysisData[self.ANALYSIS_NAME] = self.findMinAverageCoordinates(windows)
        super().analyze(imagePath)


    def rollingWindow(self, a, shape: tuple):
        """Calculates and returns rolling windows with dimensions of shape"""

        s = (a.shape[0] - shape[0] + 1,) + (a.shape[1] - shape[1] + 1,) + shape
        strides = a.strides + a.strides
        return np.lib.stride_tricks.as_strided(a, shape=s, strides=strides)
    
    def findMinAverageCoordinates(self, windows):
        """Finds the min (darkest) average of window from rolling windows list"""

        shape = windows.shape
        minAvg = 255
        minAvgCoor = {'x':0, 'y':0}
        for y in range(shape[0]):
            for x in range(shape[1]):
                avg = np.average(windows[y][x])
                if avg < minAvg:
                    minAvg = avg
                    minAvgCoor['x'] = x
                    minAvgCoor['y'] = y
        return minAvgCoor