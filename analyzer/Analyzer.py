from common.pipline.PipelineStageRunner import PipelineStageRunner
from common.pipline.FileSystemPipelineJobHandler import FileSystemPipelineJobHandler
from common import Configuration
from analyzer.AnalyzerWorker import AnalyzerWorker
from analyzer.analyzers.ImageDimensionsAnalyzer import ImageDimensionsAnalyzer
from analyzer.analyzers.DarkestLocationAnalyzer import DarkestLocationAnalyzer
from analyzer.analyzers.BrightnessIndicatorAnalyzer import BrightnessIndicatorAnalyzer
from analyzer.analyzers.AverageColorAnalyzer import AverageColorAnalyzer

analyzers = ImageDimensionsAnalyzer()
analyzers.setNext(BrightnessIndicatorAnalyzer()).setNext(AverageColorAnalyzer()).setNext(DarkestLocationAnalyzer())
worker = AnalyzerWorker(analyzers)
jobHandler = FileSystemPipelineJobHandler(worker, \
            Configuration.PROJECTED_IMAGES_DIR, \
            Configuration.ANALYSIS_RESULTS_DIR, \
            Configuration.PROCESSING_ERRORS_DIR)
runner = PipelineStageRunner()
runner.run(jobHandler)