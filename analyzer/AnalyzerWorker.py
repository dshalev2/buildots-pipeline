from common.pipline.Worker import Worker
from analyzer.AbstractAnalyzer import AbstractAnalyzer
from common.pipline.Job import Job
import json

class AnalyzerWorker(Worker):
    """The analyzer pipeline worker"""

    def __init__(self, analyzers: AbstractAnalyzer):
        self.analyzers = analyzers

    def processJob(self, job: Job):
        filename = job.fileName
        self.analyzers.analyze(filename)
        with open("{}/{}.json".format(job.outputPath, filename.split("/")[-1]), "w") as handle:
            handle.write(self.analyzers.getJsonResult())  