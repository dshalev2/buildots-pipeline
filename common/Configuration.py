# the input directory for images downloader worker
IMAGES_URLS_FILES_DIR = "workersInOut/images_urls_files"

# the output for images downloader worker and input for projector worker
DOWNLOADED_IMAGES_DIR = "workersInOut/downloaded_images"

# the output for projector worker and input for analyzer worker
PROJECTED_IMAGES_DIR = "workersInOut/projected_images"

# the output for analyzer worker
ANALYSIS_RESULTS_DIR = "workersInOut/analysis_results"

# the output for processing errors
PROCESSING_ERRORS_DIR =  "workersInOut/errors"

