
class Job:
    """A class that represent a job"""

    def __init__(self, fileName: str, outputPath: str, errorPath: str):
        self.fileName = fileName
        self.outputPath = outputPath
        self.errorPath = errorPath