
import abc

class Worker(abc.ABC):
    """An interface for a pipeline worker"""

    @abc.abstractmethod
    def processJob(self):
        pass