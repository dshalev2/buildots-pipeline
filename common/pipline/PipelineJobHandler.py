import abc
import time
import sys
import traceback
from common.pipline.Job import Job


class PipelineJobHandler(abc.ABC):
    """An abstract class for managing pipelone jobs"""

    def __init__(self, worker):
        self.worker = worker

    def run(self):
        """The main loop for every pipeline component job fetching and handling"""

        print("starting job handler main loop")
        while True:
            time.sleep(2)
            jobList = self.findWork()
            for job in jobList:
                try:
                    self.processJob(job)
                except:
                    # e = sys.exc_info()[0]
                    print("Failed to process job: {}".format(traceback.format_exc()))
                    with open("{}/{}.err".format(job.errorPath, job.fileName.split("/")[-1]), "w") as handle:
                        handle.write(traceback.format_exc())
                finally:
                    self.finishJob(job)
            
                    
    
    def processJob(self, job: Job):
        print("processing job")
        return self.worker.processJob(job)
    
    @abc.abstractmethod
    def finishJob(self, job: Job):
        pass

    @abc.abstractmethod
    def findWork(self):
        pass