from common.pipline.PipelineJobHandler import PipelineJobHandler

class PipelineStageRunner:
    """Runs the job handler for a specific pipeline stage"""

    def run(self, jobHandler: PipelineJobHandler):
        print("running pipeline stage with worker of type: {0}".format(jobHandler.worker.__class__.__name__))
        jobHandler.run()