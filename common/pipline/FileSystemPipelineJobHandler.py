
from common.pipline.PipelineJobHandler import PipelineJobHandler
from common.pipline.Job import Job
from common.pipline.Worker import Worker
import os

class FileSystemPipelineJobHandler(PipelineJobHandler):
    """A file system implementation of a pipeline job handler"""
    
    def __init__(self, worker: Worker, inputPath: str, outputPath: str, errorPath: str):
        self.inputPath = inputPath
        self.outputPath = outputPath
        self.errorPath = errorPath
        self.filesHandled = []
        self.fileErrorCount = dict()
        super().__init__(worker)


    def findWork(self) -> list:
        """Find new files to work on"""

        files = [f for f in os.listdir(self.inputPath) if not f.startswith('.')]
        freshFiles = set(files).difference(set(self.filesHandled))
        jobs = list(map(lambda f: Job(self.inputPath + "/" + f, self.outputPath, self.errorPath), freshFiles))
        for job in jobs:
            self.filesHandled.append(job.fileName)
        return jobs
        

    def finishJob(self, job: Job):
        """A Function that being called when processing of a job is done"""

        print("finished working on {0}".format(job.fileName))
        os.remove(job.fileName)
        self.filesHandled.remove(job.fileName)