from common.pipline.PipelineStageRunner import PipelineStageRunner
from common.pipline.FileSystemPipelineJobHandler import FileSystemPipelineJobHandler
from images_downloader.ImageDownloadWorker import ImageDownloaderWorker
from common import Configuration

worker = ImageDownloaderWorker()
jobHandler = FileSystemPipelineJobHandler(worker, \
            Configuration.IMAGES_URLS_FILES_DIR, \
            Configuration.DOWNLOADED_IMAGES_DIR, \
            Configuration.PROCESSING_ERRORS_DIR)
runner = PipelineStageRunner()
runner.run(jobHandler)