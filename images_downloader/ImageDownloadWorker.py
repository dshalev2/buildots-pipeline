import requests
import json
import uuid
from common.pipline.Worker import Worker
from common.pipline.Job import Job

class ImageDownloaderWorker(Worker):
    """The images downloader pipeline worker"""
    
    CONTENT_BLOCK_SIZE = 1024

    def processJob(self, job: Job):
        with open(job.fileName) as jsonFile:
            inputJson = json.load(jsonFile)
            imagesUrls = inputJson["image_list"]
        for imageUrl in imagesUrls:
            response = requests.get(imageUrl, stream=True)
                
            if not response.ok:
                msg = "unable to download image from url: {}, response: {}".format(imageUrl, response)
                print(msg)
                raise Exception("unable to download image from url: {}, response: {}".format(imageUrl, response))

            with open("{}/{}".format(job.outputPath, imageUrl.split("/")[-1]), "wb") as handle:
                for block in response.iter_content(self.CONTENT_BLOCK_SIZE):
                    if not block:
                        break
                    handle.write(block)
                